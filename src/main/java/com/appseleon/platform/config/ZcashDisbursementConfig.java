package com.appseleon.platform.config;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;



import com.appseleon.platform.cmmn.handlers.ProfileActivationHandler;
import com.appseleon.platform.cmmn.handlers.DisbursementCycleHandler;
import com.appseleon.platform.data.repository.ScaffoldingRepository;
import com.appseleon.platform.service.ContentManagementService;


@Configuration
public class ZcashDisbursementConfig {
	@Autowired
	private ContentManagementService contentManagementService;
	@Autowired
	private ScaffoldingRepository scaffoldingRepository;
	@Bean(name = "activate")
	public ProfileActivationHandler  activate() {
		return new ProfileActivationHandler();
	}
	
	@Bean(name = "hashing")
	public DisbursementCycleHandler  hashing() {
		return new DisbursementCycleHandler();
	}
	

	
	
	
	

}