package com.appseleon.platform.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;


@Configuration
@ComponentScan("com.appseleon")
public class AppSpringConfig {

	
}