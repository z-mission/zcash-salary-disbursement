package com.appseleon.platform.cmmn.handlers;



import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.springframework.beans.factory.annotation.Autowired;

import com.appseleon.platform.data.repository.ScaffoldingRepository;

import java.io.Serializable;
//import com.appseleon.zcash.services.Disbursement;
import java.security.MessageDigest;
import javax.xml.bind.DatatypeConverter;
import com.appseleon.zcash.services.Disbursement;
import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;

public class DisbursementCycleHandler implements JavaDelegate{
	@Autowired
	private ScaffoldingRepository scaffoldingRepository;
	private Disbursement disbursement;
	Disbursement DisTransaction = new Disbursement();
    public void execute(DelegateExecution execution) throws Exception {
		// TODO Auto-generated method stub
    	String id = execution.getVariable("_id").toString();
    	String pinCode = execution.getVariable("pin").toString();
    	String clientApprove = execution.getVariable("Approve").toString();
    	String MSISDN962 = execution.getVariable("MSISDN962").toString();
    	String CsvFile = execution.getVariable("file").toString();
    	String ServiceName = execution.getVariable("ServiceName").toString();
    	String CompanyName = execution.getVariable("CompanyName").toString();
    	DisbursementCycleHandler sj = new DisbursementCycleHandler();
        String HashedPin = sj.getSHA256Hash(pinCode);
        System.out.println(HashedPin);
       String Authinticate = DisTransaction.ZainCashCorporateLogin(HashedPin,ServiceName);
        if(Authinticate == "0") {
        		switch (clientApprove) {
        		case "default" :
        			Double FullAmount = DisTransaction.CollectFullAmount(CsvFile);
        			System.out.println(FullAmount);
        			String Balance = DisTransaction.getBalance(HashedPin , ServiceName);
        			Double zcashBalance = Double.parseDouble(Balance);
        			System.out.println(zcashBalance);
        			if(FullAmount <= zcashBalance) {
        				System.out.println(FullAmount);
        				DBObject DisbursmentObject = DisTransaction.ReadFile(HashedPin , CsvFile , CompanyName);
        				scaffoldingRepository.save((Serializable) DisbursmentObject, "disbursement_history");
        				Balance = DisTransaction.getBalance(HashedPin , MSISDN962);
        				zcashBalance = Double.parseDouble(Balance);
        				DBObject account = scaffoldingRepository.findOne(id, "end_user");
        				account.put("balance", zcashBalance);
        				scaffoldingRepository.save((Serializable) account, "end_user");
        			}else{
        				throw new RuntimeException("not enough balance, your zcash balance is :" + zcashBalance);
        			}
        			break;
        		case "approved"	 :
        			DBObject DisbursmentObject = DisTransaction.ReadFile(HashedPin , CsvFile , CompanyName);
    				scaffoldingRepository.save((Serializable) DisbursmentObject, "sms_history");
    				Balance = DisTransaction.getBalance(HashedPin , MSISDN962);
    				zcashBalance = Double.parseDouble(Balance);
    				DBObject account = scaffoldingRepository.findOne(id, "end_user");
    				account.put("balance", zcashBalance);
    				scaffoldingRepository.save((Serializable) account, "end_user");
        			break;	
        		case "proceed" :
        			//System.out.println(CsvFile);
        			DisTransaction.ReadJson(CsvFile ,HashedPin , CompanyName);
        			break; 
        		}	
        }else {
        	throw new RuntimeException("pin code is invalid");
        }
        
	}
    /**
     * Returns a hexadecimal encoded SHA-256 hash for the input String.
     * @param data
     * @return 
     */
    private String getSHA256Hash(String data) {
        String result = null;
        try {
            MessageDigest digest = MessageDigest.getInstance("SHA-256");
            byte[] hash = digest.digest(data.getBytes("UTF-8"));
//            System.out.println(hash); 
            
            String bytesToBase64 = new sun.misc.BASE64Encoder().encode(hash);
            return bytesToBase64;
        }catch(Exception ex) {
            ex.printStackTrace();
        }
        return result;
    }
}
