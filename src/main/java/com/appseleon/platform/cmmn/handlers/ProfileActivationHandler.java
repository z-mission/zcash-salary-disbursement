
package com.appseleon.platform.cmmn.handlers;


import com.appseleon.platform.data.repository.ScaffoldingRepository;
import java.io.Serializable;
import javax.swing.Timer;
import com.mongodb.DBObject;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.springframework.beans.factory.annotation.Autowired;
public class ProfileActivationHandler implements JavaDelegate {

	
	@Autowired
	private ScaffoldingRepository scaffoldingRepository;


    public void execute(DelegateExecution execution) throws Exception {
		// TODO Auto-generated method stub
    	String id = execution.getVariable("_id").toString();
    	DBObject account = scaffoldingRepository.findOne(id, "end_user");
    	account.put("disabled", true);
    	System.out.println(account);
		new java.util.Timer().schedule( 
		        new java.util.TimerTask() {
		            @Override
		            public void run() {
	            	scaffoldingRepository.save((Serializable) account, "end_user");
	            	DBObject account1 = scaffoldingRepository.findOne(id, "end_user");
	            	System.out.println(account1);
		            }
		        }, 
		        600000
		);
		
	}
	


} 
