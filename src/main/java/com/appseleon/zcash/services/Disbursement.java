package com.appseleon.zcash.services;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.net.*;
import java.nio.charset.StandardCharsets;
import java.io.*;

import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.bson.BSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import com.google.api.client.http.HttpResponse;
import com.mongodb.DBObject;
import com.mongodb.util.JSON;

import org.apache.http.client.config.RequestConfig;

import org.apache.http.HttpHost;

import com.mongodb.BasicDBList;
import com.mongodb.BasicDBObject;
import com.appseleon.platform.data.repository.ScaffoldingRepository;
import com.appseleon.platform.service.ContentManagementService;

import java.util.logging.Logger;

import com.appseleon.platform.web.shared.domain.transfer.ScaffoldingTransfer;

public class Disbursement {
	@Autowired

	private ContentManagementService contentService;
	@Autowired
	private ScaffoldingRepository scaffoldingRepository;
	
	public String ZainCashCorporateLogin(String pin, String ServiceName) throws IOException {
//		Map<String, String> params = new HashMap<String, String>();
//		params.put("Credentials.Password","Z@InC@$hu$eRp@$sW0Rd");
//		params.put("Credentials.Username","ZAINCASHUSR");
//		params.put("Pin", pin); 
//		params.put("ServiceName", "Zmission");
//		System.out.println(params);
//		Map<String, Object> config = new LinkedHashMap<>();
//		config.put("requestURL","http://192.168.185.66/ZainZMissionCorporateSMS/CorporateSMS.svc/ZainCashCorporateLogin");
//		config.put("requestMethod","GET");
//		config.put("entityType","UrlEncodedFormEntity");  
//		config.put("data" , params);
//		ScaffoldingTransfer response =  contentService.restCall(config);
//		Map<String, Object> data_response =  response.getSingleResult();
		String Auth = "";
		try {
			HttpPost postRequest2 = new HttpPost("http://192.168.185.66/ZainZMissionCorporateSMSTest/CorporateSMS.svc/ZainCashCorporateLogin");
			postRequest2.setHeader("Content-Type", "application/json");
			ArrayList<NameValuePair> postParameters = new ArrayList<NameValuePair>();
			   postParameters.add(new BasicNameValuePair("Credentials", "{\"Password\":\"Z@InC@$hu$eRp@$sW0Rd\",\n" + 
						"\"Username\":\"ZAINCASHUSR\"}"));
			   postParameters.add(new BasicNameValuePair("Pin", pin));
			   postParameters.add(new BasicNameValuePair("ServiceName", ServiceName));
			   postRequest2.setEntity(new UrlEncodedFormEntity(postParameters, "UTF-8"));
			   CloseableHttpClient httpClient2 = HttpClients.createDefault();
			   CloseableHttpResponse response2 = httpClient2.execute(postRequest2);
			   String responseText2 = EntityUtils.toString(response2.getEntity());
			   System.out.println(responseText2);
			   @SuppressWarnings("unchecked")
			   Map<String, String> res = (Map<String, String>) JSON.parse(responseText2);
			   Auth = res.get("Result");
			   if(Auth == "-1") {
				  throw new RuntimeException(res.get("ErrorDescription"));
			   }
				} catch (Exception e) {
					e.printStackTrace();
					//eval_content = eval_content.replace("{URL}", longURL);
				} 
				return Auth;
	} 
	
    public String getBalance(String pin,String ServiceName) {
    	String Balance = "";
		try {
			HttpPost postRequest2 = new HttpPost("http://192.168.185.66/ZainZMissionCorporateSMSTest/CorporateSMS.svc/ZainCashGetBalance");
			postRequest2.setHeader("Content-Type", "application/json");
			ArrayList<NameValuePair> postParameters = new ArrayList<NameValuePair>();
			   postParameters.add(new BasicNameValuePair("Credentials", "{\"Password\":\"Z@InC@$hu$eRp@$sW0Rd\",\n" + 
						"\"Username\":\"ZAINCASHUSR\"}"));
			   postParameters.add(new BasicNameValuePair("MSISDN962", ServiceName));
			   postParameters.add(new BasicNameValuePair("Pin", pin));
			   postRequest2.setEntity(new UrlEncodedFormEntity(postParameters, "UTF-8"));
			   CloseableHttpClient httpClient2 = HttpClients.createDefault();
			   CloseableHttpResponse response2 = httpClient2.execute(postRequest2);
			   String responseText2 = EntityUtils.toString(response2.getEntity());
			   System.out.println(responseText2);
			   @SuppressWarnings("unchecked")
			   Map<String, String> res = (Map<String, String>) JSON.parse(responseText2);
			   Balance = res.get("Result");
			   if(Balance == "-1") {
				   throw new RuntimeException(res.get("ErrorDescription"));
			   }
			   

		} catch (Exception e) {
			e.printStackTrace();
			//eval_content = eval_content.replace("{URL}", longURL);
		}
		return Balance;
	}
	
	public Double CollectFullAmount (String CsvFile) throws IOException {
		URL csvFile = null;
		String fileType = "Undetermined";
		Double fullAmount =  0.0;
		try {
			csvFile = new URL("https://storage.z-mission.com/dev-zcashportal-lc-storage-bucket/583438b60fa9b5e6a559e5ec8f11a4a3442a91542cd1a1608f41c93a4a460fa09125db9995666adf7369e0efd9ee877314365c3148a64a85a8c57bcf9608e607.csv");
			final URLConnection connection = csvFile.openConnection();
		      fileType = connection.getContentType();
		      System.out.println(fileType);
		      if("text/csv".equals(fileType)) {
		      BufferedReader reader = null;
		      try {
					reader = new BufferedReader(new InputStreamReader(csvFile.openStream()));
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		      String line = null;
			  try {
					while ((line = reader.readLine()) != null)
					{
						String[] split = line.split(",");
						String DisburseAmount = split[2];
						Double disbursement = Double.parseDouble(DisburseAmount);
						fullAmount = fullAmount + disbursement;
					}
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			  	// close our reader
			    reader.close();
			    
		      }
		      else {
		    	  System.out.println("File is not CSV");
		      }
		
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			
		}
		
		return fullAmount;
	}
	
	
	public DBObject ReadFile(String pin , String CsvFile , String CompanyName) throws IOException {
		URL csvFile = null;
		 String fileType = "Undetermined";
		 List<DBObject> DisbursementTransaction = new ArrayList<>();
		 DBObject dis = new BasicDBObject();
		try {
			csvFile = new URL("https://storage.z-mission.com/dev-zcashportal-lc-storage-bucket/583438b60fa9b5e6a559e5ec8f11a4a3442a91542cd1a1608f41c93a4a460fa09125db9995666adf7369e0efd9ee877314365c3148a64a85a8c57bcf9608e607.csv");
			final URLConnection connection = csvFile.openConnection();
		      fileType = connection.getContentType();
		      System.out.println(fileType);
		      System.out.println("test");
		      if("text/csv".equals(fileType)) {
		    	  System.out.println("test");
		      BufferedReader reader = null;
		      try {
					reader = new BufferedReader(new InputStreamReader(csvFile.openStream()));
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		      String line = null;
			  try {
			
					while ((line = reader.readLine()) != null)
					{
						String[] split = line.split(",");
						String name = split[0];
						String wallet = split[1];
						String DisburseAmount = split[2];
						System.out.println(DisburseAmount);
						DBObject DisTransStatus = new BasicDBObject();
						DisTransStatus.put("name", name);
						DisTransStatus.put("wallet", wallet);
						DisTransStatus.put("DisburseAmount", DisburseAmount);
						String DisResponse = DisburseTrans(pin, wallet,  DisburseAmount , CompanyName);
						DisTransStatus.put("status", DisResponse);
						DisbursementTransaction.add(DisTransStatus);
						System.out.println(DisbursementTransaction);
					}
					
					dis.put("transactions", DisbursementTransaction);
					dis.put("_namespace" , "zdisbursement");
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			  	// close our reader
			    reader.close();
			    
		      }
		      else {
		    	  
		      }
		
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			
		}
		return dis;
		
	}
	
	public DBObject ReadJson(String remainingWallets ,String pin , String CompanyName){
		List<DBObject> DisbursementTransaction = new ArrayList<>();
		DBObject dis = new BasicDBObject();
		BasicDBList wallets =  (BasicDBList) JSON.parse(remainingWallets);
		List<Object> res = new ArrayList<Object>();
		for(Object wallet: wallets) {
		     res.add(wallet);
		}
		for(int i =0; i< res.size();i++) {
			System.out.println(res.get(i));
			DBObject DisTransStatus = new BasicDBObject();
			Object test = res.get(i);
			String response = DisburseTrans("1234" ,((BSONObject) test).get("wallet").toString() , ((BSONObject) test).get("amount").toString() ,  CompanyName );
			//DisbursementTransaction.add(response);
			}
			
		return null;

	}
	
	public String DisburseTrans(String pin,String wallet, String DisburseAmount , String CompanyName) {
		    String response = "";
		try {
			HttpPost postRequest2 = new HttpPost("http://192.168.185.66/ZainZMissionCorporateSMSTest/CorporateSMS.svc/ZainCashBuiness2PersonSalary");
			postRequest2.setHeader("Content-Type", "application/json");
			ArrayList<NameValuePair> postParameters = new ArrayList<NameValuePair>();
			   postParameters.add(new BasicNameValuePair("Amount", DisburseAmount));
			   postParameters.add(new BasicNameValuePair("CompanyName", CompanyName));
			   postParameters.add(new BasicNameValuePair("Credentials", "{\"Password\":\"Z@InC@$hu$eRp@$sW0Rd\",\n" + 
						"\"Username\":\"ZAINCASHUSR\"}"));
			   postParameters.add(new BasicNameValuePair("Mobile", wallet));
			   postParameters.add(new BasicNameValuePair("Notes", "test"));
			   postParameters.add(new BasicNameValuePair("Pin", pin));
			   postParameters.add(new BasicNameValuePair("Sender", "Zmission"));
			   postParameters.add(new BasicNameValuePair("ShopId", "Z123"));
			   postRequest2.setEntity(new UrlEncodedFormEntity(postParameters, "UTF-8"));
			   CloseableHttpClient httpClient2 = HttpClients.createDefault();
			   CloseableHttpResponse response2 = httpClient2.execute(postRequest2);
			   String responseText2 = EntityUtils.toString(response2.getEntity());
			   System.out.println(responseText2);
			   @SuppressWarnings("unchecked")
			   Map<String, String> res = (Map<String, String>) JSON.parse(responseText2);
			   response = res.get("Result");
			   if(response == "-1"){
				   response = res.get("ErrorDescription");
			   }
			   

		} catch (Exception e) {
			e.printStackTrace();
			//eval_content = eval_content.replace("{URL}", longURL);
		}
		return response;
	}
	


}
